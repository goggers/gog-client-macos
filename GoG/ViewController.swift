//
//  ViewController.swift
//  GoG
//
//  Created by Alexander Kozlovskij on 13.07.18.
//  Copyright © 2018 gog. All rights reserved.
//

import CoreFoundation
import Cocoa
import WebKit




class ViewController: NSViewController {


	@IBOutlet weak var browser: WKWebView!;
	var ctx: UnsafeMutablePointer<GogCtx>?;
	var delegate: GogDelegate?;
	

	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Optional tests:
//		ffi_all_tests();
		rust_abi_all_tests();
		
//		var ctx:UnsafePointer<Ctx>? = Optional.none;
//		self.ctx = gog_create_context({ (error) in
		self.ctx = gog_create_context_with_printing_delegate({ (error) in
			print("(in block) gog context error.code: \(errorCode(err: error))");
			print("(in block) gog context error: \(toString(err: error))");
			// print("(in block) gog context error.description: \(errorDescription(err: error))");
		});
		
		// Optional but really needed to recive new data - set delegate:
		// self.delegate = gog_create_empty_delegate();
//		self.delegate = gog_create_printing_delegate();
//		self.ctx?.pointee.delegate = self.delegate!;
//		self.ctx?.pointee.delegate.
		
		
		
		// configure web-view:
		self.browser.allowsLinkPreview = false;
		self.browser.allowsBackForwardNavigationGestures = false;
		self.browser.addObserver(self, forKeyPath: #keyPath(WKWebView.url), options: .new, context: nil);
//		browser.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil);
		
		// initialize GoG API Client:
		if false {
			// If and when we have `client` deserialized from local db:
//			gog_import_context(ctx, <#T##client: UnsafePointer<ExportGogClient>!##UnsafePointer<ExportGogClient>!#>, <#T##err: ErrBlock!##ErrBlock!##(UnsafePointer<GogError>?) -> Void#>);
			
			gog_init_hot(ctx, {
				print("(in block) gog hot init complete");
			}, { (error) in
				print("(in block) gog hot init error: \(toString(err: error!))");
			});
			
		} else {
			
			gog_init_cold(ctx, {
				print("(in block) gog cold init complete");
			}, { (error) in
				print("(in block) gog cold init error: \(toString(err: error!))");
			});
		}
	}
	
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		if keyPath == #keyPath(WKWebView.url) {
			print("page url %: \(String(describing: browser.url))");
			
			let url = browser.url?.absoluteString;
			let is_final = gog_login_url_is_final(url, { (err) in print("gog_login_url_is_final err: \(toString(err: err))") });
			
			if is_final {
				print("User logged in. Requesting Main API Token...");
				gog_req_token_with_url(url, ctx,
				                       { print("gog req token ok") },
				                       { (err) in print("gog req token err: \(toString(err: err))") });
			}
		}
	}


	override var representedObject: Any? {
		didSet {
		// Update the view, if already loaded.
		}
	}
	
	
	
	// TODO: move into the delegate:
	public func gogClientInitialized() {
		print("gogClientInitialized");
		if gog_need_login(ctx, { (err) in print("Error in gog_need_login: \(toString(err: err))") }) {
			let url_rs = String(cString: gog_get_login_uri(ctx)!);
			self.browser.load(URLRequest(url: URL(string: url_rs)!));
		} else {
			// HERE INII-PROCESS IS TOTALLY COMPLETE!
			// do somethink cool here :)
			gogLoginComplete();
		}
	}
	
	// TODO: move into the delegate:
	public func gogLoginComplete() {
		print("gogLoginComplete");
		
		
		// ONLY FOR TEST / DEMO:
		// REMOVE NEXT:
		
		// next line for demo/tests only:
		demo_import_export();
	}
	
	// TODO: move into the delegate:
	public func gogUserInfo(/* info: */) {
		print("GET gogUserInfo");
	}
	
	
	
	
	public func demo_import_export() {
		let client = gog_export_context(self.ctx, nil);
		print("exported client: \(String(describing: client?.pointee))");
		
		let cfg = gog_export_config(nil);
		print("exported config: \(String(describing: cfg?.pointee))");
		
		// also there: gog_import_config(<#T##cfg: UnsafePointer<ExportGogConfig>!##UnsafePointer<ExportGogConfig>!#>, <#T##err: ErrBlock!##ErrBlock!##(UnsafePointer<GogError>?) -> Void#>)
		
		gog_import_context(self.ctx, client, { (err) in print("Error in gog_import_context: \(toString(err: err))") });
	}
}
